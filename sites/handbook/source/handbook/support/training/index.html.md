---
layout: handbook-page-toc
title: Support Learning & Training
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Support Learning Pathways

All the learning pathways we have in Support are listed [here](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html). Each pathway contains a set of modules, and a module may reside in multiple pathways.

To create an issue for yourself with one of the module templates:

- Click on the module you want to pursue from the [Skills by Subject](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html) page
- Title it as **_Your Name_: _Module Name_**
- Assign it to yourself
- Now you're all set to follow the instructions in the module!

### Support Onboarding Pathway

When you first join the team everything will be new to you. Don't worry! In order to get you started with GitLab quickly, apart from the company wide onboarding issue that will be assigned to you on Day 1 by PeopleOps, we also have an onboarding program in Support. Note that you can simulataneously start both the company wide onboarding and the Support Onboarding.

It is recommended that you complete the modules in the order listed, unless an issue mentions that you can start something else simultaneously. Typically, for a new team member in Support, completion of the below onboarding modules should take **6 weeks**.

**Support Engineer Onboarding Pathway**

| Module | Duration | Description |
| ------ | ------ | ------ |
| [New Support Team Member Start Here](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/New-Support-Team-Member-Start-Here.md) | 6 Weeks | Meta module to keep track of and complete all the following onbaording modules |
| [Git & GitLab Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Git-GitLab-Basics.md) | 1 Week | Understand our products and services |
| [GitLab Installation & Administration Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-Installation-Administration-Basics.md) | 1 Week | Understand the different ways in which GitLab can be installed and managed |
| [Gitlab Support Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-Support-Basics.md) | 1 Day | Understand how GitLab Support operates and the most common workflows |
| [Customer Service Skills](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Customer-Service-Skills.md) | 1 Day | Understand how we interact with customers, and how to utilize your customer service skills to ensure customer success |
| [ZenDesk Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Zendesk-Basics.md) | 1 Day | Utilize ZenDesk to perform ticket management |
| [Working on Tickets](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Working-On-Tickets.md) | 2 Weeks | Help customers by pairing with Support Engineers and replying to tickets |

**Note for new Support Engineer**

Once you complete the onboarding pathway modules, have a discussion with your manager on where your focus will be: depending on that, you have to complete either the [Self Managed Support Basics pathway](#self-managed-support-learning-pathway) **or** the [Gitlab.com SAAS Support Basics pathway](#gitlabcom-saas-support-learning-pathway). Please note that this extends your onboarding by 2 weeks, making the entire duration to be 8 weeks.

**Support Manager Onboarding Pathway**

| Module | Duration | Description |
| ------ | ------ | ------ |
| [New Support Team Member Start Here](hhttps://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/New-Support-Team-Member-Start-Here.md) | 6 Weeks | Meta module to keep track of and complete all the following onbaording modules |
| [Support Manager Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Support-Manager-Basics.md) | 2 Weeks | Understand support management processes and workflows |
| [Git & GitLab Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Git-GitLab-Basics.md) | 1 Week | Understand our products and services |
| [Gitlab Support Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-Support-Basics.md) | 1 Day | Understand how GitLab Support operates and the most common workflows |
| [ZenDesk Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Zendesk-Basics.md) | 1 Day | Utilize ZenDesk to perform ticket management |

#### Reference Table for Achievable Progress - First 6 Months

Our onboarding pathway gives new Support Engineers an opportunity to learn at their own pace and explore. We strongly believe in learning by doing ([70/20/10 learning model](https://trainingindustry.com/wiki/content-development/the-702010-model-for-learning-and-development/)), and encourage Support Engineers to start contributing on tickets with public or internal comments from as early as your 3rd week. 

To help with that, the following reference table was built based on data, and can be used as a guideline on what they can aim to achieve in terms of getting comfortable with ticket management in their first 6 months in GitLab Support. It can also be used by managers as a reference on where they can expect to see their new hire in their first 6 months.

Note: The data is by month - for instance, in my 4th month as a Dotcom support engineer, I can aim to make 60+ comments on tickets.

| Month | Public Comments Reference Range - Self Managed | Public Comments Reference Range - Dotcom |
| ------ |  ------ | ------ |
| 1 | 0-10 | 0-20 |
| 2 | 10-30 | 20-40 |
| 3 | 30-50 | 40-60 |
| 4 | 50+ | 60+ |
| 5 | 60+ | 80+ |
| 6 | 70+ | 100+ |

### GitLab.com SAAS Support Learning Pathway

| Module | Duration | Description |
| ------ | ------ | ------ |
| [GitLab.com Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-com-Basics.md) | 2 Weeks | Understand the basics to answer GitLab.com (SaaS) product related tickets |
| [GitLab.com CMOC](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-com-CMOC.md) | 1 Day | Understand the responsibilities of being the [Communications Manager On Call (CMOC)](/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) for an active GitLab.com incident |

### Self Managed Support Learning Pathway

| Module | Duration | Description |
| ------ | ------ | ------ |
| [Self Managed Support Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Self-Managed-Basics.md) | 2 Weeks | Understand the basics to answer GitLab.com (SaaS) product related tickets |
| [Customer Emergency](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Customer-Emergencies.md) | 1 Week | Understand the responsibilities of being on-call for Customer Emergencies |

## Support Training Project

We are continuously working on adding more modules and building out more learning pathways: you can find a list of all our current training modules under the [Support Training project](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates).

Please submit an MR if you have suggestions on how this page could be better!
